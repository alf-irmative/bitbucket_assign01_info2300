## Project Owner

Peter Saunders

Created for info2300

---

## Install Project

Installing this project is simple.

1. Click **Source** on the left side.
2. Download the file **index.html**.
3. Open the file in your browser.
---

## License Information

This project is provided as is and I or other creators take no responsiblity for issues that it may contain. This software is now part of the public domain.

For more information review the license at [choosealicense.org](https://choosealicense.com/licenses/unlicense/)

We have chosen this license since we do not believe that the content of this project is unique or of strong commercial value to others. However should one choose to use the algorithms enclosed we believe that you should be able to do so without encumbrance.